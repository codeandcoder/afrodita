# README #

This is the Front-End project of a Spanish speaking community webpage called Animagens.es

### How do I get set up? ###
You will need to install node.js 5.x.x and npm 3.x.x in your machine.

Then you just have to clone the repository, open a console inside the recently downloaded folder and type "npm install" to download all the modules that this project uses.

Then, a simple "npm start" will deploy the webpage on your local machine.

The major issue here is that you will need to connect with the back-end project in order to get it working. For that, you can go here and run it too:
https://bitbucket.org/codeandcoder/afrodita

### Authors ###
Santi Ruiz <starfly1570@gmail.com> <codeandcoder.net>