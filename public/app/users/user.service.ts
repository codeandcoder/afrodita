
import { Injectable, Inject } from '@angular/core';

import { SocketService } from '../sockets/socket.service';

@Injectable()
export class UsersService {
    users = {};

    getUser(u_id: any) {
        if (!this.users) this.users = {};
        if (!this.users[u_id]) {
            this.socketService.send('request-user', {user_id: u_id});
        }
    }

    constructor(@Inject(SocketService) private socketService: SocketService) {
        this.users = {};
        socketService.addListener('user-info', (user: any) => {
            this.users[user._id] = user;
        });
    }
}