"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var config_service_1 = require('../config/config.service');
var SocketService = (function () {
    function SocketService(config) {
        var _this = this;
        this.config = config;
        this.recoveringSession = false;
        this.socket = io(config.socketUrl);
        this.session_token = parseCookies(document.cookie).session_token;
        this.socket.on('session', function (data) {
            _this.recoveringSession = false;
            _this.myUser = data.user;
        });
        this.socket.on('disconnect', function (data) {
            location.reload();
        });
        this.socket.on('logout', function (data) {
            location.reload();
        });
    }
    SocketService.prototype.addListener = function (key, f) {
        this.socket.on(key, f);
    };
    SocketService.prototype.send = function (op, data) {
        if (op == 'request-session') {
            if (!this.recoveringSession) {
                this.recoveringSession = true;
                this.socket.emit(op, data);
            }
        }
        else {
            this.socket.emit(op, data);
        }
    };
    SocketService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [config_service_1.ConfigService])
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
function parseCookies(rc) {
    var list = {};
    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}
//# sourceMappingURL=socket.service.js.map