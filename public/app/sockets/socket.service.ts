import { Injectable } from '@angular/core';

import { User } from '../users/user.model';
import { ConfigService } from '../config/config.service';

@Injectable()
export class SocketService {
    socket: any;
    session_token: string;
    myUser: any;
    recoveringSession = false;

    addListener(key: string, f: Function) {
        this.socket.on(key, f);
    }

    send(op: string, data: any) {
        if (op == 'request-session') {
            if (!this.recoveringSession) {
                this.recoveringSession = true;
                this.socket.emit(op, data);
            }
        } else {
            this.socket.emit(op, data);
        }
    }

    constructor(private config: ConfigService) {
        this.socket = io(config.socketUrl);

        this.session_token = parseCookies(document.cookie).session_token;

        this.socket.on('session', (data: any) => {
            this.recoveringSession = false;
            this.myUser = data.user;
        });
        
        this.socket.on('disconnect', (data: any) => {
            location.reload();
        });

        this.socket.on('logout', (data: any) => {
            location.reload();
        });
    }
}

function parseCookies (rc: string) : any {
    var list = {};

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}