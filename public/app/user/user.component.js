"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var common_1 = require('@angular/common');
var ng2_file_upload_1 = require('ng2-file-upload');
var config_service_1 = require('../config/config.service');
var socket_service_1 = require('../sockets/socket.service');
var nav_component_1 = require('../nav/nav.component');
var menu_item_model_1 = require('../nav/menu-item.model');
var UserComponent = (function () {
    function UserComponent(socketService, params, zone, fb, config) {
        this.socketService = socketService;
        this.params = params;
        this.zone = zone;
        this.fb = fb;
        this.config = config;
        this.menuItems = [
            new menu_item_model_1.MenuItem('Inicio', 'Home', 'fa-home', false),
            new menu_item_model_1.MenuItem('Chat', 'Chat', 'fa-comments-o', false),
            new menu_item_model_1.MenuItem('Foro', 'Forum', 'fa-globe', false)
        ];
        this.primaryColor = "darkblue";
        this.isEdit = false;
        this.imagesUrl = this.config.imagesUrl;
        this.uploader = new ng2_file_upload_1.FileUploader({ url: this.config.imagesUploadUrl });
        this.submitted = false;
    }
    UserComponent.prototype.logout = function () {
        this.socketService.send('request-logout', {});
    };
    UserComponent.prototype.onSubmit = function (value) {
        value._id = this.user._id;
        if (this.uploader.queue[0]) {
            value.image = this.imagesUrl + '/' + this.user.nickname;
        }
        else {
            value.image = this.user.image;
        }
        this.socketService.send('update-user', value);
        this.submitted = true;
    };
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.addListener('user-updated', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.submitted = false;
                    _this.error = {
                        type: 'danger',
                        msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                    };
                }
                else {
                    location.reload();
                }
            });
        });
        this.socketService.addListener('user-info', function (user) {
            _this.zone.run(function () {
                _this.user = user;
                if (_this.user._id == _this.socketService.myUser._id) {
                    _this.isEdit = true;
                    _this.form = _this.fb.group({
                        alias: [_this.user.alias, common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(4)])],
                        color: [_this.user.color],
                        sound: [_this.user.sound],
                        confidential: [_this.user.confidential]
                    });
                }
            });
        });
        this.socketService.addListener('session', function (data) {
            if (data.err || !data.user) {
                document.location.href = '/login';
            }
            else {
                _this.socketService.send('request-user', { nickname: _this.params.get('u') });
            }
        });
        this.socketService.send('request-session', { session_token: this.socketService.session_token });
    };
    UserComponent = __decorate([
        core_1.Component({
            selector: 'amgs-user',
            templateUrl: "app/user/user.template.html",
            styleUrls: ["app/user/user.style.css"],
            directives: [common_1.FORM_DIRECTIVES, ng2_file_upload_1.FILE_UPLOAD_DIRECTIVES, nav_component_1.NavComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, router_deprecated_1.RouteParams, core_2.NgZone, common_1.FormBuilder, config_service_1.ConfigService])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map