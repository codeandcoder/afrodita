
import { Component, OnInit } from '@angular/core';
import { FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/common';
import { NgZone } from '@angular/core';
import {ReCaptchaComponent} from 'angular2-recaptcha/angular2-recaptcha';
import { SocketService } from '../sockets/socket.service';

import { Register } from './register.model';

@Component({
  selector: 'amgs-register-form',
  templateUrl: 'app/register/register-form.template.html',
  styleUrls: ['app/register/register-form.style.css'],
  directives: [FORM_DIRECTIVES, ReCaptchaComponent],
  providers: []
})
export class RegisterFormComponent implements OnInit {
    form: any;
    submitted = false;
    successful = false;
    genderOptions: any[] = [
        {value: 'unknown', name: 'Sin especificar'},
        {value: 'male', name: 'Hombre'},
        {value: 'female', name: 'Mujer'}
    ];
    error: any;
    captcha: any;
    captcha_resolved = false;
    handleCorrectCaptcha(event: any) {
        this.zone.run(() => {
            this.captcha_resolved = true;
        });
    }

    onSubmit(value: any) {
        this.error = undefined;
        if (value.rep_email != value.email) {
            this.error = {
                type: 'warning',
                msg: 'Las direcciones de correo electrónico no coinciden.'
            }
        } else if (value.rep_password != value.password) {
            this.error = {
                type: 'warning',
                msg: 'Las passwords no coinciden.'
            }
        } else if (new Date().getTime() - new Date(value.birth).getTime() < 1000 * 60 * 60 * 24 * 365 * 2) {
            this.error = {
                type: 'warning',
                msg: 'No podemos permitir el registro de recién nacidos; por favor, introduce tu edad real.'
            }
        } else {
            this.submitted = true;
            this.socketService.send('request-register', value);
        }
    }

    ngOnInit() {
        this.form = this.fb.group({
            nickname: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            alias: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            email: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            rep_email: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(7)])],
            rep_password: ['', Validators.compose([Validators.required, Validators.minLength(7)])],
            birth: ['2016-01-01', Validators.required],
            gender: ['unknown'],
            terms: ['']
        });

        this.socketService.addListener('register', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.submitted = false;
                    if (data.err.code == 101) {
                        this.error = {
                            type: 'danger',
                            msg: 'El nombre de usuario introducido ya existe. Por favor, escoge uno diferente.'
                        };
                    } else if (data.err.code == 102) {
                        this.error = {
                            type: 'danger',
                            msg: 'El correo electrónico introducido ya está registrado.'
                        };
                    } else {
                        this.error = {
                            type: 'danger',
                            msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                        };
                    }
                } else {
                    this.successful = true;
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err) {
            } else if (data.user) {
                document.location.href = '/';
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token});
    }

    constructor(private socketService: SocketService, private zone: NgZone, private fb: FormBuilder) {
    }
}