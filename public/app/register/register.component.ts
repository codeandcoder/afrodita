import { Component} from '@angular/core';

import { RegisterFormComponent } from './register-form.component';

@Component({
    selector: 'amgs-register',
    template: '<amgs-register-form>Loading...</amgs-register-form>',
    directives: [RegisterFormComponent],
    providers: []
})
export class RegisterComponent {
}