import { Component } from '@angular/core';

@Component({
    selector: 'amgs-terms',
    templateUrl: 'app/terms/terms.template.html',
    styleUrls: ['app/terms/terms.style.css'],
    directives: []
})
export class TermsComponent {}