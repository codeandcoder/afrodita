import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/common';
import { NgZone } from '@angular/core';

import { SocketService } from '../sockets/socket.service';

@Component({
  selector: 'amgs-recovery',
  templateUrl: 'app/recovery/recovery.template.html',
  styleUrls: ['app/recovery/recovery.style.css'],
  providers: []
})
export class RecoveryComponent implements OnInit {
    model = {
        email: ''
    };
    submitted = false;
    error: any;
    successful = false;

    onSubmit() { 
        this.submitted = true;
        this.socketService.send('request-recovery', {
            email: this.model.email
        })
    }

    ngOnInit() {
        this.socketService.addListener('recovery', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.submitted = false;
                    if (data.err.code == 117) {
                        this.error = {
                            type: 'danger',
                            msg: 'El correo introducido no está registrado en nuestro sistema.'
                        }
                    } else {
                        this.error = {
                            type: 'danger',
                            msg: 'No se ha podido recuperar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                        }
                    }
                } else {
                    this.successful = true;
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err) {
            } else  if (data.user) {
                document.location.href = '/';
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token});
    }

    constructor(private socketService: SocketService, private zone: NgZone) {
    }
}