"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var socket_service_1 = require('../sockets/socket.service');
var RecoveryComponent = (function () {
    function RecoveryComponent(socketService, zone) {
        this.socketService = socketService;
        this.zone = zone;
        this.model = {
            email: ''
        };
        this.submitted = false;
        this.successful = false;
    }
    RecoveryComponent.prototype.onSubmit = function () {
        this.submitted = true;
        this.socketService.send('request-recovery', {
            email: this.model.email
        });
    };
    RecoveryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.addListener('recovery', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.submitted = false;
                    if (data.err.code == 117) {
                        _this.error = {
                            type: 'danger',
                            msg: 'El correo introducido no está registrado en nuestro sistema.'
                        };
                    }
                    else {
                        _this.error = {
                            type: 'danger',
                            msg: 'No se ha podido recuperar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                        };
                    }
                }
                else {
                    _this.successful = true;
                }
            });
        });
        this.socketService.addListener('session', function (data) {
            if (data.err) {
            }
            else if (data.user) {
                document.location.href = '/';
            }
        });
        this.socketService.send('request-session', { session_token: this.socketService.session_token });
    };
    RecoveryComponent = __decorate([
        core_1.Component({
            selector: 'amgs-recovery',
            templateUrl: 'app/recovery/recovery.template.html',
            styleUrls: ['app/recovery/recovery.style.css'],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone])
    ], RecoveryComponent);
    return RecoveryComponent;
}());
exports.RecoveryComponent = RecoveryComponent;
//# sourceMappingURL=recovery.component.js.map