import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
    socketUrl = config.socketUrl;
    imagesUploadUrl = config.imagesUploadUrl;
    imagesUrl = config.imagesUrl;

    constructor() {
    }
}