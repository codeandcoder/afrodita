"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var nav_component_1 = require('../nav/nav.component');
var header_component_1 = require('../header/header.component');
var LayoutComponent = (function () {
    function LayoutComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], LayoutComponent.prototype, "menuItems", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], LayoutComponent.prototype, "primaryColor", void 0);
    LayoutComponent = __decorate([
        core_1.Component({
            selector: 'amgs-layout',
            templateUrl: 'app/layout/layout.template.html',
            styleUrls: ['app/layout/layout.style.css'],
            directives: [nav_component_1.NavComponent, header_component_1.HeaderComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], LayoutComponent);
    return LayoutComponent;
}());
exports.LayoutComponent = LayoutComponent;
//# sourceMappingURL=layout.component.js.map