import { Component, Input } from '@angular/core';
import { NgZone } from '@angular/core';

import { SocketService } from '../sockets/socket.service';
import { NavComponent } from '../nav/nav.component';
import { HeaderComponent } from '../header/header.component';
import { MenuItem } from '../nav/menu-item.model';
import {UsersService} from '../users/user.service';

@Component({
    selector: 'amgs-layout',
    templateUrl: 'app/layout/layout.template.html',
    styleUrls: ['app/layout/layout.style.css'],
    directives: [NavComponent, HeaderComponent]
})
export class LayoutComponent {
    @Input()
    menuItems: MenuItem[];
    @Input()
    primaryColor: string;
}