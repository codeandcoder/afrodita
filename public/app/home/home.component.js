"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var common_1 = require('@angular/common');
var ng2_ckeditor_1 = require('ng2-ckeditor');
var socket_service_1 = require('../sockets/socket.service');
var layout_component_1 = require('../layout/layout.component');
var menu_item_model_1 = require('../nav/menu-item.model');
var user_service_1 = require('../users/user.service');
var HomeComponent = (function () {
    function HomeComponent(socketService, zone, usersService, fb) {
        var _this = this;
        this.socketService = socketService;
        this.zone = zone;
        this.usersService = usersService;
        this.fb = fb;
        this.users = this.usersService.users;
        this.menuItems = [
            new menu_item_model_1.MenuItem('Inicio', 'Home', 'fa-home', true),
            new menu_item_model_1.MenuItem('Chat', 'Chat', 'fa-comments-o', false),
            new menu_item_model_1.MenuItem('Foro', 'Forum', 'fa-globe', false)
        ];
        this.primaryColor = "blue";
        this.submitted = false;
        this.form = this.fb.group({
            title: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(4)])]
        });
        this.socketService.addListener('blogs', function (data) {
            if (data.err) {
                _this.error = {
                    type: 'danger',
                    msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                };
            }
            else {
                _this.zone.run(function () {
                    _this.blogs = data.blogs;
                });
            }
        });
        this.socketService.addListener('blog-created', function (data) {
            if (data.err) {
                _this.submitted = false;
                _this.error = {
                    type: 'danger',
                    msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                };
            }
            else {
                location.reload();
            }
        });
        this.socketService.addListener('session', function (data) {
            if (data.err || !data.user) {
                document.location.href = '/login';
            }
            else {
                _this.socketService.send('request-blogs', {});
                _this.zone.run(function () {
                    _this.myUser = data.user;
                });
            }
        });
    }
    HomeComponent.prototype.onSubmit = function (value) {
        this.submitted = true;
        this.socketService.send('create-blog', {
            title: value.title,
            content: this.content
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'amgs-home',
            templateUrl: 'app/home/home.template.html',
            styleUrls: ['app/home/home.style.css'],
            directives: [layout_component_1.LayoutComponent, ng2_ckeditor_1.CKEditor]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, user_service_1.UsersService, common_1.FormBuilder])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map