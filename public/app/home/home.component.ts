import { Component } from '@angular/core';
import { NgZone } from '@angular/core';
import { FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/common';
import {CKEditor} from 'ng2-ckeditor';

import { SocketService } from '../sockets/socket.service';
import { LayoutComponent } from '../layout/layout.component';
import { MenuItem } from '../nav/menu-item.model';
import {UsersService} from '../users/user.service';

@Component({
    selector: 'amgs-home',
    templateUrl: 'app/home/home.template.html',
    styleUrls: ['app/home/home.style.css'],
    directives: [LayoutComponent, CKEditor]
})
export class HomeComponent {
    users = this.usersService.users;
    myUser: any;
    menuItems = [
        new MenuItem('Inicio', 'Home', 'fa-home', true),
        new MenuItem('Chat', 'Chat', 'fa-comments-o', false),
        new MenuItem('Foro', 'Forum', 'fa-globe', false)
    ];
    primaryColor = "blue";
    blogs: any;
    submitted = false;
    error: any;
    form: any;
    content: any;

    onSubmit(value: any) {
        this.submitted = true;
        this.socketService.send('create-blog', {
            title: value.title,
            content: this.content
        });
    }

    constructor(private socketService: SocketService, private zone: NgZone, private usersService: UsersService, private fb: FormBuilder) {
        this.form = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.minLength(4)])]
        });
        this.socketService.addListener('blogs', (data: any) => {
            if (data.err) {
                this.error = {
                    type: 'danger',
                    msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                };
            } else {
                this.zone.run(() => {
                    this.blogs = data.blogs;
                });
            }
        });
        this.socketService.addListener('blog-created', (data: any) => {
            if (data.err) {
                this.submitted = false;
                this.error = {
                    type: 'danger',
                    msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                };
            } else {
                location.reload();
            }
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err || !data.user) {
                document.location.href = '/login';
            } else {
                this.socketService.send('request-blogs', {});
                this.zone.run(() => {
                    this.myUser = data.user;
                });
            }
        });
    }
}