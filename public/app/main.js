"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_component_1 = require('./app.component');
var socket_service_1 = require('./sockets/socket.service');
var config_service_1 = require('./config/config.service');
var user_service_1 = require('./users/user.service');
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [socket_service_1.SocketService, config_service_1.ConfigService, user_service_1.UsersService]);
//# sourceMappingURL=main.js.map