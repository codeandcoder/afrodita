import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/common';
import { FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/common';
import { NgZone } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { SocketService } from '../sockets/socket.service';

@Component({
  selector: 'amgs-rec',
  templateUrl: 'app/rec/rec.template.html',
  styleUrls: ['app/rec/rec.style.css'],
  providers: [],
  directives: [FORM_DIRECTIVES]
})
export class RecComponent implements OnInit {
    form: any;
    error: any;
    successful = false;
    submitted = false;
    user = {nickname: 'test'};

    onSubmit(value: any) {
        this.error = undefined;
        if (value.rep_password != value.password) {
            this.error = {
                type: 'warning',
                msg: 'Las passwords no coinciden.'
            }
        } else {
            this.submitted = true;
            this.socketService.send('update-rec-user-password', {
                token: this.params.get('token'),
                password: value.password
            });
        }
        return true;
    }

    ngOnInit() {
        this.form = this.fb.group({
            password: ['', Validators.compose([Validators.required, Validators.minLength(7)])],
            rep_password: ['', Validators.compose([Validators.required, Validators.minLength(7)])]
        });

        this.socketService.addListener('user-password-updated', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.submitted = false;
                    this.error = {
                        type: 'danger',
                        msg: 'No se ha podido recuperar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                    }
                } else {
                    this.user = data.user;
                    this.successful = true;
                }
            });
        });
        this.socketService.addListener('recovery-processed', (data: any) => {
            this.zone.run(() => {
                if (data.err || !data.user) {
                    this.error = {
                        type: 'danger',
                        msg: 'No se ha podido recuperar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                    }
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err) {
            } else if (data.user) {
                document.location.href = '/';
            } else {
                this.socketService.send('process-recovery', {
                    token: this.params.get('token')
                });
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token});
    }

    constructor(private socketService: SocketService, private zone: NgZone, private params: RouteParams, private fb: FormBuilder) {
    }
}