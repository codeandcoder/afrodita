import { Component } from '@angular/core';
import { NgZone } from '@angular/core';

@Component({
    selector: 'amgs-header',
    templateUrl: 'app/header/header.template.html',
    styleUrls: ['app/header/header.style.css'],
    directives: []
})
export class HeaderComponent {
}