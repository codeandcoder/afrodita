export class MenuItem {
    title: string;
    link: string;
    icon: string;
    active: boolean;

    constructor(title: string, link: string, icon: string, active: boolean) {
        this.title = title;
        this.link = link;
        this.icon = icon;
        this.active = active;
    }
}