"use strict";
var MenuItem = (function () {
    function MenuItem(title, link, icon, active) {
        this.title = title;
        this.link = link;
        this.icon = icon;
        this.active = active;
    }
    return MenuItem;
}());
exports.MenuItem = MenuItem;
//# sourceMappingURL=menu-item.model.js.map