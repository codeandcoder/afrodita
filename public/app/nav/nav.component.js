"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var socket_service_1 = require('../sockets/socket.service');
var NavComponent = (function () {
    function NavComponent(socketService, zone, router) {
        this.socketService = socketService;
        this.zone = zone;
        this.router = router;
        this.statuses = [
            'connected',
            'navigating',
            'bussy',
            'disconnected'
        ];
    }
    NavComponent.prototype.getStatusTitle = function () {
        if (this.myChatUser) {
            switch (this.myChatUser.status) {
                case 'connected':
                    return 'Conectado';
                case 'navigating':
                    return 'Navegando';
                case 'bussy':
                    return 'Ocupado';
                case 'disconnected':
                    return 'Desconectado';
            }
        }
        return '';
    };
    NavComponent.prototype.changeStatus = function () {
        var newIndex = (this.statuses.indexOf(this.myChatUser.status) + 1) % this.statuses.length;
        this.myChatUser.status = this.statuses[newIndex];
        this.socketService.send('update-chat-user', { status: this.myChatUser.status });
    };
    NavComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.addListener('chat-user', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.myChatUser = {
                        status: 'navigating'
                    };
                }
                else {
                    _this.myChatUser = data.chatUser;
                }
            });
        });
        this.socketService.addListener('session', function (data) {
            if (data.err || !data.user) {
                document.location.href = '/login';
            }
            else {
                _this.zone.run(function () {
                    _this.myUser = data.user;
                    _this.socketService.send('request-chat-user', {});
                });
            }
        });
        this.socketService.send('request-session', { session_token: this.socketService.session_token, page: location.pathname });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], NavComponent.prototype, "menuItems", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], NavComponent.prototype, "primaryColor", void 0);
    NavComponent = __decorate([
        core_1.Component({
            selector: 'amgs-nav',
            templateUrl: 'app/nav/nav.template.html',
            styleUrls: ['app/nav/nav.style.css'],
            directives: [router_deprecated_1.RouterLink],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, router_deprecated_1.Router])
    ], NavComponent);
    return NavComponent;
}());
exports.NavComponent = NavComponent;
//# sourceMappingURL=nav.component.js.map