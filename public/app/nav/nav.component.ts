import { Component, Input, OnInit } from '@angular/core';
import { NgZone } from '@angular/core';
import { RouterLink, Router } from '@angular/router-deprecated';

import { SocketService } from '../sockets/socket.service';
import { MenuItem } from './menu-item.model';

@Component({
    selector: 'amgs-nav',
    templateUrl: 'app/nav/nav.template.html',
    styleUrls: ['app/nav/nav.style.css'],
    directives: [RouterLink],
    providers: []
})
export class NavComponent implements OnInit {
    myUser: any;
    @Input()
    menuItems: MenuItem[];
    @Input()
    primaryColor: string;
    myChatUser: any;

    statuses = [
        'connected',
        'navigating',
        'bussy',
        'disconnected'
    ];

    getStatusTitle() {
        if (this.myChatUser) {
            switch(this.myChatUser.status) {
                case 'connected':
                    return 'Conectado';
                case 'navigating':
                    return 'Navegando';
                case 'bussy':
                    return 'Ocupado';
                case 'disconnected':
                    return 'Desconectado';        
            }
        }
        return '';
    }

    changeStatus() {
        var newIndex = (this.statuses.indexOf(this.myChatUser.status)+1) % this.statuses.length;
        this.myChatUser.status = this.statuses[newIndex];
        this.socketService.send('update-chat-user', {status: this.myChatUser.status});
    }

    ngOnInit() {
        this.socketService.addListener('chat-user', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.myChatUser = {
                        status: 'navigating'
                    };
                } else {
                    this.myChatUser = data.chatUser;
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err || !data.user) {
                document.location.href = '/login';
            } else {
                this.zone.run(() => {
                    this.myUser = data.user;
                    this.socketService.send('request-chat-user', {});
                });
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token, page: location.pathname});
    }

    constructor(private socketService: SocketService, private zone: NgZone, private router: Router) {
    }
}