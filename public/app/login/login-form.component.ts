import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/common';
import { NgZone } from '@angular/core';

import { SocketService } from '../sockets/socket.service';

import { Login } from './login.model';

@Component({
  selector: 'amgs-login-form',
  templateUrl: 'app/login/login-form.template.html',
  styleUrls: ['app/login/login-form.style.css'],
  providers: []
})
export class LoginFormComponent implements OnInit {
    model = new Login('', '');
    submitted = false;
    error: any;

    onSubmit() { 
        this.submitted = true;
        this.socketService.send('request-login', {
            nickname: this.model.nickname,
            password: this.model.password
        })
    }

    ngOnInit() {
        this.socketService.addListener('login', (data: any) => {
            if (data.err) {
                this.submitted = false;
                if (data.err.code == 117 || data.err.code == 109) {
                    this.error = {
                        type: 'danger',
                        msg: 'Los datos de usuario introducido son incorrectos.'
                    };
                } else if (data.err.code == 110) {
                    this.error = {
                        type: 'danger',
                        msg: 'El usuario introducido aún no ha sido verificado. Revisa tu bandeja de correo electrónico y la carpeta de Spam del mismo.'
                    };
                } else {
                    this.error = {
                        type: 'danger',
                        msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                    };
                }
            } else {
                document.cookie = "session_token=" + data.session_token;
                document.location.href = '/';
            }
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err) {
            } else  if (data.user) {
                document.location.href = '/';
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token});
    }

    constructor(private socketService: SocketService, private zone: NgZone) {
    }
}