import { Component} from '@angular/core';

import { LoginFormComponent } from './login-form.component';

@Component({
    selector: 'amgs-login',
    template: '<amgs-login-form>Loading...</amgs-login-form>',
    directives: [LoginFormComponent],
    providers: []
})
export class LoginComponent {
}