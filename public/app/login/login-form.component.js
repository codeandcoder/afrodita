"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var socket_service_1 = require('../sockets/socket.service');
var login_model_1 = require('./login.model');
var LoginFormComponent = (function () {
    function LoginFormComponent(socketService, zone) {
        this.socketService = socketService;
        this.zone = zone;
        this.model = new login_model_1.Login('', '');
        this.submitted = false;
    }
    LoginFormComponent.prototype.onSubmit = function () {
        this.submitted = true;
        this.socketService.send('request-login', {
            nickname: this.model.nickname,
            password: this.model.password
        });
    };
    LoginFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.addListener('login', function (data) {
            if (data.err) {
                _this.submitted = false;
                if (data.err.code == 117 || data.err.code == 109) {
                    _this.error = {
                        type: 'danger',
                        msg: 'Los datos de usuario introducido son incorrectos.'
                    };
                }
                else if (data.err.code == 110) {
                    _this.error = {
                        type: 'danger',
                        msg: 'El usuario introducido aún no ha sido verificado. Revisa tu bandeja de correo electrónico y la carpeta de Spam del mismo.'
                    };
                }
                else {
                    _this.error = {
                        type: 'danger',
                        msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                    };
                }
            }
            else {
                document.cookie = "session_token=" + data.session_token;
                document.location.href = '/';
            }
        });
        this.socketService.addListener('session', function (data) {
            if (data.err) {
            }
            else if (data.user) {
                document.location.href = '/';
            }
        });
        this.socketService.send('request-session', { session_token: this.socketService.session_token });
    };
    LoginFormComponent = __decorate([
        core_1.Component({
            selector: 'amgs-login-form',
            templateUrl: 'app/login/login-form.template.html',
            styleUrls: ['app/login/login-form.style.css'],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone])
    ], LoginFormComponent);
    return LoginFormComponent;
}());
exports.LoginFormComponent = LoginFormComponent;
//# sourceMappingURL=login-form.component.js.map