"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var layout_component_1 = require('../layout/layout.component');
var menu_item_model_1 = require('../nav/menu-item.model');
var active_conversation_component_1 = require('./conversation/active-conversation.component');
var socket_service_1 = require('../sockets/socket.service');
var list_component_1 = require('./list/list.component');
var user_service_1 = require('../users/user.service');
var ChatComponent = (function () {
    function ChatComponent(usersService, socketService, zone) {
        var _this = this;
        this.usersService = usersService;
        this.socketService = socketService;
        this.zone = zone;
        this.users = this.usersService.users;
        this.menuItems = [
            new menu_item_model_1.MenuItem('Inicio', 'Home', 'fa-home', false),
            new menu_item_model_1.MenuItem('Chat', 'Chat', 'fa-comments-o', true),
            new menu_item_model_1.MenuItem('Foro', 'Forum', 'fa-globe', false)
        ];
        this.primaryColor = "orange";
        this.conversations = [];
        this.chatUsers = {};
        this.chatUsersList = [];
        this.starreds = [];
        this.rooms = [];
        socketService.addListener('starreds', function (data) {
            if (data.err) {
            }
            else {
                _this.starreds = data.starreds;
                for (var i = 0; i < data.starreds.length; i++) {
                    if (!_this.users[data.starreds[i]]) {
                        _this.usersService.getUser(data.starreds[i]);
                    }
                }
            }
        });
        socketService.addListener('chat-users', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.chatUsers = {};
                    _this.chatUsersList = [];
                }
                else {
                    _this.chatUsers = data.chatUsers;
                    _this.chatUsersList = Object.keys(_this.chatUsers);
                    for (var i = 0; i < _this.chatUsersList.length; i++) {
                        _this.usersService.getUser(_this.chatUsersList[i]);
                    }
                }
            });
        });
        this.socketService.addListener('session', function (data) {
            if (data.err || !data.user) {
                document.location.href = '/login';
            }
            else {
                socketService.send('request-chat-users', {});
                socketService.send('request-starreds', {});
                _this.zone.run(function () {
                    _this.myUser = data.user;
                });
            }
        });
    }
    ChatComponent.prototype.selectConversation = function (index) {
        this.activeConversation = this.conversations[index];
    };
    ChatComponent.prototype.newConversation = function (data) {
        if (data.type == 'user') {
            if (!this.users[data.user_id]) {
                this.usersService.getUser(data.user_id);
            }
            this.activeConversation = { type: 'user', data: { _id: parseInt(data.user_id) } };
            var index = this.findConversationIndex({ type: 'user', data: { _id: parseInt(data.user_id) } });
            if (index < 0) {
                this.conversations.push({ type: 'user', data: { _id: parseInt(data.user_id) } });
            }
        }
    };
    ChatComponent.prototype.createRoom = function (data) {
    };
    ChatComponent.prototype.createUserConversation = function (data) {
    };
    ChatComponent.prototype.makeStarred = function () {
        if (this.starreds.indexOf(this.activeConversation.data._id) < 0) {
            this.socketService.send('make-starred', { user_id: this.activeConversation.data._id });
        }
        else {
            this.socketService.send('make-unstarred', { user_id: this.activeConversation.data._id });
        }
    };
    ChatComponent.prototype.findConversationIndex = function (params) {
        for (var i = 0; i < this.conversations.length; i++) {
            var c = this.conversations[i];
            if (c.type == params.type && c.data._id == params.data._id) {
                return i;
            }
        }
        return -1;
    };
    ChatComponent = __decorate([
        core_1.Component({
            selector: 'amgs-chat',
            template: "\n        <amgs-layout [menuItems]=\"menuItems\" [primaryColor]=\"primaryColor\">Loading...</amgs-layout>\n        <amgs-active-conversation *ngIf=\"activeConversation\" [starreds]=\"starreds\" [chatUsers]=\"chatUsers\" [conversation]=\"activeConversation\" (makeStarred)=\"makeStarred($event)\">Loading...</amgs-active-conversation>\n        <amgs-chat-list [myUser]=\"myUser\" [conversations]=\"conversations\" [chatUsers]=\"chatUsers\" [chatUsersList]=\"chatUsersList\" [starreds]=\"starreds\" [rooms]=\"rooms\" (newConversation)=\"newConversation($event)\" (selectConversation)=\"selectConversation($event)\" (createRoom)=\"createRoom($event)\" (createUserConversation)=\"createUserConversation($event)\">Loading...</amgs-chat-list>\n    ",
            directives: [layout_component_1.LayoutComponent, active_conversation_component_1.ActiveConversationComponent, list_component_1.ChatListComponent]
        }), 
        __metadata('design:paramtypes', [user_service_1.UsersService, socket_service_1.SocketService, core_2.NgZone])
    ], ChatComponent);
    return ChatComponent;
}());
exports.ChatComponent = ChatComponent;
//# sourceMappingURL=chat.component.js.map