import { Component } from '@angular/core';
import { NgZone } from '@angular/core';

import { LayoutComponent } from '../layout/layout.component';
import { MenuItem } from '../nav/menu-item.model';

import { ActiveConversationComponent } from './conversation/active-conversation.component';

import { SocketService } from '../sockets/socket.service';
import { ChatListComponent } from './list/list.component';
import {UsersService} from '../users/user.service';

@Component({
    selector: 'amgs-chat',
    template: `
        <amgs-layout [menuItems]="menuItems" [primaryColor]="primaryColor">Loading...</amgs-layout>
        <amgs-active-conversation *ngIf="activeConversation" [starreds]="starreds" [chatUsers]="chatUsers" [conversation]="activeConversation" (makeStarred)="makeStarred($event)">Loading...</amgs-active-conversation>
        <amgs-chat-list [myUser]="myUser" [conversations]="conversations" [chatUsers]="chatUsers" [chatUsersList]="chatUsersList" [starreds]="starreds" [rooms]="rooms" (newConversation)="newConversation($event)" (selectConversation)="selectConversation($event)" (createRoom)="createRoom($event)" (createUserConversation)="createUserConversation($event)">Loading...</amgs-chat-list>
    `,
    directives: [LayoutComponent, ActiveConversationComponent, ChatListComponent]
})
export class ChatComponent {
    myUser:any;
    users = this.usersService.users;
    menuItems = [
        new MenuItem('Inicio', 'Home', 'fa-home', false),
        new MenuItem('Chat', 'Chat', 'fa-comments-o', true),
        new MenuItem('Foro', 'Forum', 'fa-globe', false)
    ];
    primaryColor = "orange";
    activeConversation: any;
    conversations: any[] = [];
    chatUsers: any = {};
    chatUsersList: any[] = [];
    starreds: any[] = [];
    rooms: any[] = [];

    selectConversation(index:any) {
        this.activeConversation = this.conversations[index];
    }

    newConversation(data:any) {
        if (data.type == 'user') {
            if (!this.users[data.user_id]) {
                this.usersService.getUser(data.user_id);
            }
            this.activeConversation = {type: 'user', data: {_id: parseInt(data.user_id)}};
            var index = this.findConversationIndex({type: 'user', data: {_id: parseInt(data.user_id)}});
            if (index < 0) {
                this.conversations.push({type: 'user', data: {_id: parseInt(data.user_id)}});
            }
        }
    }
    
    createRoom(data:any) {

    }

    createUserConversation(data:any) {

    }

    makeStarred() {
        if (this.starreds.indexOf(this.activeConversation.data._id) < 0) {
            this.socketService.send('make-starred', {user_id: this.activeConversation.data._id});
        } else {
            this.socketService.send('make-unstarred', {user_id: this.activeConversation.data._id});
        }
    }

    constructor(private usersService: UsersService, private socketService: SocketService, private zone: NgZone) {
        socketService.addListener('starreds', (data: any) => {
            if (data.err) {
                // do nothing
            } else {
                this.starreds = data.starreds;
                for (let i = 0; i < data.starreds.length; i++) {
                    if (!this.users[data.starreds[i]]) {
                        this.usersService.getUser(data.starreds[i]);
                    }
                }
            }
        });
        socketService.addListener('chat-users', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.chatUsers = {};
                    this.chatUsersList = [];
                } else {
                    this.chatUsers = data.chatUsers;
                    this.chatUsersList = Object.keys(this.chatUsers);
                    for (var i = 0; i < this.chatUsersList.length; i++) {
                        this.usersService.getUser(this.chatUsersList[i]);
                    }
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err || !data.user) {
                document.location.href = '/login';
            } else {
                socketService.send('request-chat-users', {});
                socketService.send('request-starreds', {});
                this.zone.run(() => {
                    this.myUser = data.user;
                });
            }
        });
    }

    findConversationIndex(params:any): number {
        for (let i = 0; i < this.conversations.length; i++) {
            var c = this.conversations[i];
            if (c.type == params.type && c.data._id == params.data._id) {
                return i;
            }
        }
        return -1;
    }
}