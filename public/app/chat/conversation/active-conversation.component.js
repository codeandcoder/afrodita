"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('../../users/user.service');
var ActiveConversationComponent = (function () {
    function ActiveConversationComponent(usersService) {
        this.usersService = usersService;
        this.users = this.usersService.users;
        this.starreds = [];
        this.makeStarred = new core_1.EventEmitter();
    }
    ActiveConversationComponent.prototype.starred = function () {
        this.makeStarred.next({});
    };
    ActiveConversationComponent.prototype.ngOnChanges = function (changes) {
        console.log(changes);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ActiveConversationComponent.prototype, "conversation", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ActiveConversationComponent.prototype, "chatUsers", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], ActiveConversationComponent.prototype, "starreds", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ActiveConversationComponent.prototype, "makeStarred", void 0);
    ActiveConversationComponent = __decorate([
        core_1.Component({
            selector: 'amgs-active-conversation',
            templateUrl: 'app/chat/conversation/active-conversation.template.html',
            styleUrls: ['app/chat/conversation/active-conversation.style.css']
        }), 
        __metadata('design:paramtypes', [user_service_1.UsersService])
    ], ActiveConversationComponent);
    return ActiveConversationComponent;
}());
exports.ActiveConversationComponent = ActiveConversationComponent;
//# sourceMappingURL=active-conversation.component.js.map