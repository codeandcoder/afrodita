var neodb = require('../neo4jdb');

// BLOG
exports.findBlogs = function(callback) {
  neodb.findBlogs(function(err, blogs) {
    return callback(err, blogs);
  });
};

exports.createBlog = function(title, content, user_id, callback) {
  neodb.createBlog(title, content, user_id, function(err) {
    return callback(err);
  });
};

// USERS
exports.findUserById = function(user_id, callback) {
  neodb.findUserById(user_id, function(err, user) {
    return callback(err, user);
  });
};

exports.findUserByNickname = function(nickname, callback) {
  neodb.findUserByNickname(nickname, function(err, user) {
    return callback(err, user);
  });
};

exports.findUserByEmail = function(email, callback) {
  neodb.findUserByEmail(email, function(err, user) {
    return callback(err, user);
  });
};

exports.findUserVerification = function(nickname, callback) {
  neodb.findUserVerification(nickname, function(err, verification) {
    return callback(err, verification);
  });
};

exports.findUserAuthorization = function(nickname, callback) {
  neodb.findUserAuthorization(nickname, function(err, authorization) {
    return callback(err, authorization);
  });
};

exports.findUserInfo = function(user_id, callback) {
  neodb.findUserInfo(user_id, function(err, info) {
    return callback(err, info);
  });
};

exports.findUserPerms = function(user_id, callback) {
  neodb.findUserPerms(user_id, function(err, perms) {

    /* TODO: find ROOMS, POSTS, TOPICS and COMMENTS created today and rest.
    var now = new Date();
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    var timestamp = startOfDay / 1000;*/

    return callback(err, perms);
  });
};

exports.findUserConfig = function(user_id, callback) {
  neodb.findUserConfig(user_id, function(err, config) {
    return callback(err, config);
  });
};

exports.saveUser = function(user, callback) {
  neodb.saveUser(user, function(err) {
    return callback(err);
  });
};

exports.updateUserPassword = function(user_id, newPassword, callback) {
  neodb.updateUserPassword(user_id, newPassword, function(err) {
    return callback(err);
  });
};

exports.verifyUser = function(token, callback) {
  neodb.verifyUser(token, function(err) {
    return callback(err);
  })
};

exports.findUserList = function(callback) {
  neodb.findUserList(function(err, users) {
    return callback(err, users);
  });
};

// RECOVERY
exports.createRecovery = function(email, token, callback) {
  neodb.createRecovery(email, token, function(err) {
    return callback(err);
  });
};

exports.findRecovery = function(token, callback) {
  neodb.findRecovery(token, function(err, user) {
    return callback(err, user);
  });
};

exports.removeRecovery = function(token, callback) {
  neodb.removeRecovery(token, function(err) {
    return callback(err);
  });
};

// SESSIONS
exports.findSessionUsers = function(callback) {
  neodb.findSessionUsers(function(err, users) {
    return callback(null, users);
  });
};

exports.checkSession = function(session_token, callback) {
  neodb.checkSession(session_token, function(err, user) {
    if (user) {
      neodb.findUserConfig(user._id, function(err, config) {
        user.config = config;
        neodb.findUserPerms(user._id, function(err, perms) {
          user.perms = perms;
          return callback(err, user);
        });
      });
    } else {
      return callback(err, user);
    }
  });
};

exports.createSession = function(session, callback) {
  neodb.createSession(session, function(err) {
    return callback(err);
  });
};

exports.removeSession = function(user_id, callback) {
  neodb.removeSession(user_id, function(err) {
    return callback(err);
  });
};

exports.expireSessions = function(timeLimit, callback) {
  neodb.expireSessions(timeLimit, function(err) {
    return callback(err);
  });
};

// CHAT - STARRED
exports.findStarredUsers = function(user_id, callback) {
  neodb.findStarredUsers(user_id, function(err, starreds) {
    return callback(err, starreds);
  });
};

exports.setStarred = function(user_id, target_id, callback) {
  neodb.findStarredUsers(user_id, function(err, starreds) {
    if (starreds.indexOf(parseInt(target_id)) < 0) {
      neodb.setStarred(user_id, target_id, function(err) {
        return callback(err);
      });
    }
  });
};

exports.setUnstarred = function(user_id, target_id, callback) {
  neodb.setUnstarred(user_id, target_id, function(err) {
    return callback(err);
  });
};

// CHAT - ROOMS
exports.findChatRooms = function(callback) {
  neodb.findChatRooms(function(err, rooms) {
    return callback(err, rooms);
  });
};

exports.saveChatRoom = function(room, callback) {
  neodb.saveChatRoom(room, function(err) {
    return callback(err);
  });
};

// CHAT - MEESSAGES
exports.saveChatMessage = function(message, callback) {
  neodb.saveChatMessage(message, function(err) {
    return callback(err);
  });
};

exports.findChatMessages = function(user_id, target_id, from, amount, callback) {
  neodb.findChatMessages(user_id, target_id, from, amount, function(err, messages) {
    return callback(err, messages);
  });
};

exports.savePendingMessage = function(message, callback) {
  neodb.savePendingMessage(message, function(err) {
    return callback(err);
  });
};

exports.retrievePendingMessages = function(user_id, callback) {
  neodb.retrievePendingMessages(user_id, function(err, conversations) {
    return callback(err, conversations);
  });
};

exports.fixChatMessages = function(callback) {
  neodb.fixChatMessages(function(err) {
    return callback(err);
  });
};