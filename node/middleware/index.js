var logger = require('../logger');
var config = require('../config').vars();
var utils = require('../utils');

var version = '2.0';

exports.work = function (req, res, next) {
	var ip = req.headers['x-forwarded-for'] === undefined ? req.connection.remoteAddress : req.headers['x-forwarded-for'];
    var session_token = req.session.session_token === undefined ? req.body.SessionToken : req.session.session_token;
	if (req.headers.host.match(/^www/) !== null ) {
		res.redirect('https://' + req.headers.host.replace(/^www\./, '') + req.url);
	} else {
        var pageUrl = req.url.indexOf('?') > -1 ? req.url.substring(1, req.url.indexOf('?')) : req.url.substring(1);
        var context = {
            title: 'Animagens',
            version: version,
            ip: ip,
            test: config.test,
            navHistory: [],
            page: pageUrl,
            pageColor: utils.getPageColor(pageUrl)
        };
        utils.checkSession(session_token, function(err, user) {
            context.user = user;
            req.animagens = {
                context: context
            };
            try {
                next();
            } catch (err) {
                logger.error(err);
            }
        });
    }
};