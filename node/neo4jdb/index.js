var config = require('../config').vars();
var logger = require('../logger');
var neo4j = require('node-neo4j');
var db = new neo4j(config.neodb_url);
var utils = require('../utils');

// BLOG
exports.findBlogs = function(callback) {
  db.cypherQuery('MATCH (b:Blog)-[rel:POSTED]-(u:User) return b, id(u), rel.timestamp order by rel.timestamp', function(err, results) {
    if (err) return callback(err);

    var blogs = [];
    if (results.data.length) {
      blogs = results.data.map(function(r) {
        var blog = r[0];
        blog.user_id = r[1];
        blog.created_at = r[2];
        return blog;
      });
    }

    return callback(null, blogs);
  });
};

exports.createBlog = function(title, content, user_id, callback) {
  db.cypherQuery('MATCH (u:User) where id(u) = {user_id} CREATE (u)-[rel:POSTED {timestamp:TIMESTAMP()}]->(p:Blog {title:{title}, content:{content}})', {user_id:user_id, title:title, content:content}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

// USERS
exports.findUserById = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User:Active) where id(u) = {user_id} RETURN u', {user_id: user_id}, function(err, results) {
    if (err) return callback(err);

    var user;
    if (results.data.length) {
      user = results.data[0];
    }

    return callback(null, user);
  });
};

exports.findUserByNickname = function(nickname, callback) {
  db.cypherQuery('MATCH (u:User:Active {nickname:{nickname}}) RETURN u', {nickname: nickname}, function(err, results) {
    if (err) return callback(err);

    var user;
    if (results.data.length) {
      user = results.data[0];
    }

    return callback(null, user);
  });
};

exports.findUserByEmail = function(email, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[r:HAS_INFO]->(i {email:{email}}) RETURN u', {email: email}, function(err, results) {
    if (err) return callback(err);

    var user;
    if (results.data.length) {
      user = results.data[0];
    }

    return callback(null, user);
  });
};

exports.findUserVerification = function(nickname, callback) {
  db.cypherQuery('MATCH (u:User:Active {nickname:{nickname}})-[r:HAS_VERIFICATION]->(v:Verified) RETURN v', {nickname: nickname}, function(err, results) {
    if (err) return callback(err);

    var verification;
    if (results.data.length) {
      verification = results.data[0];
    }

    return callback(null, verification);
  });
};

exports.findUserAuthorization = function(nickname, callback) {
  db.cypherQuery('MATCH (u:User:Active {nickname:{nickname}})-[r:HAS_AUTHORIZATION]->(a) RETURN a', {nickname: nickname}, function(err, results) {
    if (err) return callback(err);

    var authorization;
    if (results.data.length) {
      authorization = results.data[0];
    }

    return callback(null, authorization);
  });
};

exports.findUserInfo = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[r:HAS_INFO]->(i:Info) WHERE id(u) = {user_id} RETURN i', {user_id: user_id}, function(err, results) {
    if (err) return callback(err);

    var info;
    if (results.data.length) {
      info = results.data[0];
    }

    return callback(null, info);
  });
};

exports.findUserPerms = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[rel:HAS_ROLE]->(r:ROLE) WHERE id(u) = {user_id} RETURN r', {user_id: user_id}, function(err, results) {
    if (err) return callback(err);

    var perms = {
      writeBlog: false,
      createRooms: 0,
      createTopics: 0,
      createPosts: 0,
      createComments: 0,
      deleteTopics: false,
      deletePosts: false,
      deleteComments: false,
      manageRooms: false,
      banUsers: false,
      isAdmin: false,
      createPrivateTopics: false,
      createPrivatePosts: false
    };
    for(var i = 0; i < results.data.length; i++) {
      var role = results.data[i];
      perms.writeBlog = perms.writeBlog || role.writeBlog;
      perms.createRooms = perms.createRooms > role.createRooms ? perms.createRooms : role.createRooms;
      perms.createTopics = perms.createTopics > role.createTopics ? perms.createTopics : role.createTopics;
      perms.createPosts = perms.createPosts > role.createPosts ? perms.createPosts : role.createPosts;
      perms.createComments = perms.createComments > role.createComments ? perms.createComments : role.createComments;
      perms.deleteTopics = perms.deleteTopics || role.deleteTopics;
      perms.deletePosts = perms.deletePosts || role.deletePosts;
      perms.deleteComments = perms.deleteComments || role.deleteComments;
      perms.manageRooms = perms.manageRooms || role.manageRooms;
      perms.banUsers = perms.banUsers || role.banUsers;
      perms.isAdmin = perms.isAdmin || role.isAdmin;
      perms.createPrivateTopics = perms.createPrivateTopics || role.createPrivateTopics;
      perms.createPrivatePosts = perms.createPrivatePosts || role.createPrivatePosts;
    }

    return callback(null, perms);
  });
};

exports.saveUser = function(user, callback) {
  if (user._id) {
    db.cypherQuery('MATCH (u:User:Active) WHERE id(u) = {user}._id'
        + ' SET u.alias = {user}.alias,'
        + ' u.confidential = {user}.confidential,'
        + ' u.image = {user}.image,'
        + ' u.color = {user}.color'
    , {user:user}, function(err) {
        if (err) return callback(err);
          db.cypherQuery('MATCH (u:User:Active)-[r:HAS_CONFIG]-(c:Config) WHERE id(u) = {user}._id'
              + ' SET c.sound = {user}.config.sound'
          , {user: user}, function(err) {
            if (err) return callback(err);
            return callback(null);
          });
    });
  } else {
    db.cypherQuery('CREATE (u:User:Active {image:"https://jamfnation.jamfsoftware.com/img/default-avatars/generic-user.png",'
        + 'color:"#000000", nickname:{user}.nickname, alias:{user}.alias, created_at:TIMESTAMP(), confidential: true})', {user: user}, function(err) {
      if (err) return callback(err);
      db.cypherQuery('MATCH (u:User:Active {nickname:{user}.nickname}) CREATE (u)-[r:HAS_CONFIG]->(c:Config {sound:true})', {user: user}, function(err) {
        if (err) return callback(err);
        db.cypherQuery('MATCH (u:User:Active {nickname:{user}.nickname}) CREATE (u)-[r:HAS_INFO]->(i:Info {email:{user}.email, birth:{user}.birth, gender:{user}.gender})', {user: user}, function(err) {
          if (err) return callback(err);
          db.cypherQuery('MATCH (u:User:Active {nickname:{user}.nickname}) CREATE (u)-[r:HAS_AUTHORIZATION]->(a:Authorization {password:{user}.password, salt:{user}.salt})', {user: user}, function(err) {
            if (err) return callback(err);
            db.cypherQuery('MATCH (u:User:Active {nickname:{user}.nickname}) CREATE (u)-[r:HAS_VERIFICATION]->(v:Verification {token:{user}.verification_token})', {user: user}, function(err) {
              if (err) return callback(err);
              return callback(null);
            });
          });
        });
      });
    });
  }
};

exports.updateUserPassword = function(user_id, newPassword, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[r:HAS_AUTHORIZATION]->(a:Authorization) WHERE id(u) = {user_id} SET a.password = {newPassword}', {user_id: user_id, newPassword: newPassword}, function(err) {
    return callback(err);
  });
}

exports.verifyUser = function(token, callback) {
  db.cypherQuery('MATCH (v:Verification {token:{token}}) SET v:Verified', {token: token}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.findUserConfig = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[r:HAS_CONFIG]->(c:Config) WHERE id(u) = {user_id} RETURN c', {user_id: user_id}, function(err, results) {
    if (err) return callback(err);

    var config;
    if (results.data.length) {
      config = results.data[0];
    }

    return callback(null, config);
  });
};

exports.findUserList = function(callback) {
  db.cypherQuery('MATCH (u:User:Active) RETURN {_id: id(u), nickname: u.nickname, alias: u.alias}', function(err, results) {
    if (err) return callback(err);
    return callback(null, results.data);
  });
};

// RECOVERY
exports.createRecovery = function(email, token, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[rel:HAS_INFO]-(i) where i.email = {email} CREATE (s:Recovery {token:{token}})<-[r:HAS_RECOVERY {timestamp:TIMESTAMP()}]-(u)', {email:email, token:token}, function(err, results) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.findRecovery = function(token, callback) {
  db.cypherQuery('MATCH (s:Recovery {token:{token}})-[r]-(u) RETURN u', {token: token}, function(err, results) {
    if (err) return callback(err);
    
    var user;
    if (results.data.length) {
      user = results.data[0];
    }

    return callback(null, user);
  });
};

exports.removeRecovery = function(token, callback) {
  db.cypherQuery('MATCH (r:Recovery) where r.token = {token} DETACH DELETE r', {token: token}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

// SESSIONS
exports.checkSession = function(session_token, callback) {
  db.cypherQuery('MATCH (s:Session {token:{session_token}})-[r]-(u) RETURN u', {session_token: session_token}, function(err, results) {
    if (err) return callback(err);

    var user;
    if (results.data.length) {
      user = results.data[0];
      db.cypherQuery('MATCH (s:Session {token:{session_token}})-[r]-(u) SET r.timestamp = TIMESTAMP()', {session_token: session_token}, function(err, results) {
        if (err) logger.error(err);
      });
    }

    return callback(null, user);
  });
};

exports.findSessionUsers = function(callback) {
  db.cypherQuery('MATCH (s:Session)-[r]-(u) return id(u)', function(err, results) {
    if (err) return callback(err);
    return callback(null, results.data);
  });
};

exports.createSession = function(session, callback) {
  db.cypherQuery('MATCH (u:User:Active) where id(u) = {session}.user_id CREATE (s:Session {token:{session}.token})<-[r:HAS_SESSION {timestamp:TIMESTAMP()}]-(u)', {session: session}, function(err, results) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.removeSession = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User:Active)-[r:HAS_SESSION]->(s:Session) where id(u) = {user_id} DETACH DELETE s', {user_id: user_id}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.expireSessions = function(timeLimit, callback) {
  db.cypherQuery('MATCH (s:Session)-[r:HAS_SESSION]-(u) WHERE r.timestamp < {limit} DETACH DELETE s', {limit:(Date.now() - timeLimit)}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

// CHAT - STARRED
exports.findStarredUsers = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User)-[rel:HAS_STARRED]-(u2:User) WHERE id(u) = {user_id} RETURN id(u2)', {user_id:user_id}, function(err, results) {
    if (err) return callback(err);
    return callback(null, results.data);
  });
};

exports.setStarred = function(user_id, target_id, callback) {
  db.cypherQuery('MATCH (u:User), (u2:User) WHERE id(u) = {user_id} AND id(u2) = {target_id} CREATE (u)-[rel:HAS_STARRED]->(u2)', {user_id:parseInt(user_id), target_id:parseInt(target_id)}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.setUnstarred = function(user_id, target_id, callback) {
  db.cypherQuery('MATCH (u:User)-[rel:HAS_STARRED]->(u2:User) WHERE id(u) = {user_id} AND id(u2) = {target_id} DETACH DELETE rel', {user_id:parseInt(user_id), target_id:parseInt(target_id)}, function(err) {
    console.log(err);
    if (err) return callback(err);
    return callback(null);
  });
};

// CHAT - ROOMS
exports.findChatRooms = function(callback) {
  db.cypherQuery('MATCH (r:ChatRoom:Active)-[o:OWNS_CHAT_ROOM]-(u) RETURN r,id(u),o.timestamp ORDER BY r.name', function(err, results) {
    if (err) return callback(err);

    var rooms;
    if (results.data.length) {
      rooms = results.data.map(function(r) {
        var room = r[0];
        room.user_id = r[1];
        room.created_at = r[2];
        return room;
      });
    }

    return callback(null, rooms);
  });
};

exports.saveChatRoom = function(room, callback) {
  if (room._id) {
    db.cypherQuery('MATCH (r:ChatRoom:Active) WHERE id(r) = {room}._id'
      + ' SET r.name = {room}.name,'
      + ' r.perms = [{room}.perms.join()],'
      + ' r.bans = [{room}.bans.join()],'
      + ' r.public = {room}.public'
    , {room:room}, function(err) {
        if (err) return callback(err);
        return callback(null);
    });
  } else {
    db.cypherQuery('MATCH (u:User) where id(u) = {room}.user_id CREATE (r:ChatRoom:Active {'
    + 'name:{room}.name, perms: [{room}.perms.join()], bans: [{room}.bans.join()], '
    + 'public:{room}.public'
    + '})<-[o:OWNS_CHAT_ROOM {timestamp:TIMESTAMP()}]-(u)', {room:room} , function(err, results) {
      if (err) return callback(err);
      return callback(null);
    });
  }
};

// CHAT - MESSAGES
exports.saveChatMessage = function(message, callback) {
  db.cypherQuery('MATCH (u:User), (t) WHERE id(u) = {message}.user_id AND id(t) = {message}.target_id CREATE (u)-[r:SENT_MESSAGE {timestamp:TIMESTAMP()}]->(m:ChatMessage {content:{message}.content}), (t)-[r2:GOT_MESSAGE {timestamp:TIMESTAMP()}]->(m)', {message:message}, function(err, results) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.findChatMessages = function(user_id, target_id, from, amount, callback) {
  if (user_id) {
    db.cypherQuery('MATCH (u:User)-[r:SENT_MESSAGE]->(m:ChatMessage)<-[r2:GOT_MESSAGE]-(t:User) WHERE id(u) = {user_id} AND id(t) = {target_id} OR id(t) = {user_id} AND id(u) = {target_id} RETURN m, id(u), id(t), r.timestamp ORDER BY r.timestamp DESC SKIP {from} LIMIT {amount}', {user_id:user_id, target_id:target_id, from:from, amount: amount}, function(err, results) {
      if (err) return callback(err);

      formatMessageResults(results, function(err, messages) {
        return callback(null, messages);
      });
    });
  } else {
    db.cypherQuery('MATCH (u:User)-[r:SENT_MESSAGE]->(m:ChatMessage)<-[r2:GOT_MESSAGE]-(t:ChatRoom) WHERE id(t) = {target_id} RETURN m, id(u), id(t), r.timestamp ORDER BY r.timestamp DESC SKIP {from} LIMIT {amount}', {target_id:target_id, from:from, amount: amount}, function(err, results) {
      if (err) return callback(err);

      formatMessageResults(results, function(err, messages) {
        return callback(null, messages);
      });
    });
  }
};

exports.savePendingMessage = function(message, callback) {
  db.cypherQuery('MATCH (u:User), (t:User) WHERE id(u) = {message}.user_id AND id(t) = {message}.target_id CREATE (u)-[r:SENT_MESSAGE {timestamp:TIMESTAMP()}]->(m:PendingMessage {content:{message}.content}), (t)-[r2:GOT_MESSAGE {timestamp:TIMESTAMP()}]->(m)', {message:message}, function(err) {
    if (err) return callback(err);
    return callback(null);
  });
};

exports.retrievePendingMessages = function(user_id, callback) {
  db.cypherQuery('MATCH (u:User)-[r:SENT_MESSAGE]->(m:PendingMessage)<-[r2:GOT_MESSAGE]-(t:User) WHERE id(t) = {user_id} RETURN m, id(u), id(t), r.timestamp ORDER BY r.timestamp DESC', {user_id:user_id}, function(err, results) {
    if (err) return callback(err);

    formatMessageResults(results, function(err, messages) {
      var conversations = {};
      for (var i = 0; i < messages.length; i++) {
        var m = messages[i];
        if(!conversations[m.user_id]) {
          conversations[m.user_id] = {
            messages: []
          }
        }
        conversations[m.user_id].messages.push(m);
      }

      db.cypherQuery('MATCH (m:PendingMessage)<-[r2:GOT_MESSAGE]-(t:User) WHERE id(t) = {user_id} REMOVE m:PendingMessage SET m:ChatMessage', {user_id:user_id}, function(err) {if (err) logger.error(err)});

      return callback(null, enlistConversations(conversations));
    });
  });
};

function formatMessageResults(results, callback) {
  var messages = [];
  for (var i = 0; i < results.data.length; i++) {
    var data = results.data[i];
    var m = data[0];
    m.user_id = data[1];
    m.target_id = data[2];
    m.created_at = data[3];
    messages.push(m);
  }
  return callback(null, messages.reverse());
};

function enlistConversations(conversations) {
  var list = [];
  var keys = Object.keys(conversations);
  for (var i = 0; i < keys.length; i++) {
    var c = {
      conversation: {
        type: 'personal',
        user_id: keys[i]
      },
      messages: conversations[keys[i]].messages
    };
    list.push(c);
  }
  return list;
}

function e(data) {
  return data.replace(/"/g,"\"");
}