var db = require('../db');
var logger = require('../logger');
var utils = require('../utils');
var constants = require('../constants');
var enotifier = require('../enotifier');

exports.config = function(socket) {
    socket.on('request-blogs', function(data) {
        db.findBlogs(function(err, blogs) {
            socket.emit('blogs', {blogs: blogs});
        });
    });

    socket.on('create-blog', function(data) {
        if (socket.user) {
            db.createBlog(data.title, data.content, socket.user._id, function(err) {
                socket.emit('blog-created', {err: err});
            });
        }
    });
};