var db = require('../../db');
var logger = require('../../logger');
var utils = require('../../utils');
var constants = require('../../constants');
var enotifier = require('../../enotifier');

var pageSockets = {};
var userSockets = {};
var sendToAllSockets;

var chatUsers = {};

exports.init = function(pSockets, uSockets, staSockets) {
    pageSockets = pSockets;
    userSockets = uSockets;
    sendToAllSockets = staSockets;

    setInterval(function() {
        /*var obj = {};
        var keys = Object.keys(userSockets);
        for (var i = 0; i < keys.length; i++) {
            var sockets = userSockets[keys[i]];
            var sKeys = Object.keys(sockets);
            obj[keys[i]] = sKeys;
        }
        console.log(obj);*/
        console.log(chatUsers);
    }, 2000);
};

exports.config = function(socket) {
    socket.on('request-chat-users', function(data) {
        return socket.emit('chat-users', {chatUsers: filterUsers(chatUsers)});
    });

    socket.on('request-chat-user', function(data) {
        if (socket.user) {
            return socket.emit('chat-user', {chatUser: chatUsers[socket.user._id]});
        }
    });

    socket.on('update-chat-user', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            chatUsers[socket.user._id].status = data.status;
            sendUsersUpdate();
        }
    });

    socket.on('request-starreds', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            db.findStarredUsers(socket.user._id, function(err, starreds) {
                return socket.emit('starreds', {err: err, starreds: starreds});
            });
        }
    });

    socket.on('make-starred', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            db.setStarred(socket.user._id, data.user_id, function(err) {
                db.findStarredUsers(socket.user._id, function(err, starreds) {
                    return socket.emit('starreds', {err: err, starreds: starreds});
                });
            });
        }
    });

    socket.on('make-unstarred', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            db.setUnstarred(socket.user._id, data.user_id, function(err) {
                db.findStarredUsers(socket.user._id, function(err, starreds) {
                    return socket.emit('starreds', {err: err, starreds: starreds});
                });
            });
        }
    });
};

exports.connectUser = function(user) {
    if (!chatUsers[user._id]) {
        chatUsers[user._id] = {
            status: 'navigating'
        }
        sendUsersUpdate();
    } else {
        logger.error('TWO SAME USER IN CHAT!!');
    }
};

exports.userEnteredChat = function(user) {
    if (chatUsers[user._id]) {
        if (chatUsers[user._id].status == 'navigating') {
            chatUsers[user._id].status = 'connected';
            sendUsersUpdate();
        }
    };
};

exports.userExitedChat = function(user) {
    if (chatUsers[user._id] && chatUsers[user._id].status == 'connected') {
        var found = false;
        var keys = Object.keys(userSockets[user._id]);
        for (var i = 0; i < keys.length && !found; i++) {
            var s = userSockets[user._id][keys[i]];
            if (s.page == '/chat') {
                found = true;
            }
        }
        if (!found) {
            chatUsers[user._id].status = 'navigating';
            sendUsersUpdate();
        }
    };
};

exports.disconnectUser = function(user) {
    delete chatUsers[user._id];
    sendUsersUpdate();
};

function prepareUserList(users) {
    var userList = [];
    var keys = Object.keys(users);
    for (var i = 0; i < keys.length; i++) {
        var u = {
            _id: keys[i],
            status: users[keys[i]].status
        }
        userList.push(u);
    }
    return userList;
}

function sendUsersUpdate() {
    var keys = Object.keys(chatUsers);
    for (var i = 0; i < keys.length; i++) {
        sendToAllSockets(userSockets[keys[i]], 'chat-users', {chatUsers: filterUsers(chatUsers)});
        sendToAllSockets(userSockets[keys[i]], 'chat-user', {chatUser: chatUsers[keys[i]]});
    }
}

function filterUsers() {
    var filteredUsers = {};
    var keys = Object.keys(chatUsers);
    for (var i = 0; i < keys.length; i++) {
        if (chatUsers[keys[i]].status != 'disconnected') {
            filteredUsers[keys[i]] = chatUsers[keys[i]];
        }
    }
    return filteredUsers;
}