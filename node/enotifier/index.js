var nodemailer = require('nodemailer');
var config = require('../config').vars('config.json');

var smtpTransport = nodemailer.createTransport("SMTP", {
  service: "Gmail",
  auth: {
    XOAuth2: {
      user: "soporte.animagens@gmail.com",
      clientId: "655190985765-7lj4riqiof5m397pefbdsguco71dhbj0.apps.googleusercontent.com",
      clientSecret: config.mail_secret,
      refreshToken: config.mail_refresh
    }
  }
});

exports.sendMail = function(data, callback) {
	var mailOptions = {
	  from: 'soporte@animagens.es',
	  to: data.mails.join(),
	  subject: data.subject,
	  generateTextFromHTML: true,
	  html: data.content
	};

	smtpTransport.sendMail(mailOptions, function(err, response) {
		smtpTransport.close();
		if (err) {
			return callback(err);
		} else {
			return callback(null);
		}
	});
};