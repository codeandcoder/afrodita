var db = require('../db');
var logger = require('../logger');
var refreshInterval = 1000 * 60;
var expiringSessionTime = 1000 * 60 * 60 * 12;

exports.init = function() {
  setInterval(function() {
    db.expireSessions(expiringSessionTime, function(err) {
      if (err) {
        logger.error(err);
      }
    });
  }, refreshInterval);
};