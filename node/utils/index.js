var linkify = require('linkifyjs');
var config = require('../config').vars();
var constants = require('../constants');
//var mongodb = require('../mongodb');
var db = require('../db');

var pageColors = {
    '': 'rgba(40, 129, 187, 0.97)',
    'bienvenida': 'rgba(7, 153, 215, 0.97)',
    'literatura': 'rgba(165, 222, 55, 0.97)',
    'literatura-nueva': 'rgba(165, 222, 55, 0.97)',
    'literatura-editar': 'rgba(165, 222, 55, 0.97)',
    'analisis': 'rgba(111, 195, 135, 0.97)',
    'analisis-nuevo': 'rgba(111, 195, 135, 0.97)',
    'analisis-editar': 'rgba(111, 195, 135, 0.97)',
    'concursos': 'rgba(240, 197, 20, 0.97)',
    'proyectos': 'rgba(254, 174, 27, 0.97)'
};
exports.getPageColor = function(page) {
    if (!pageColors[page]) {
        return 'rgba(40, 129, 187, 0.97)';
    }
    return pageColors[page];
};

exports.checkSession = function (session_token, callback) {
    if (!session_token) return callback(constants.getError('c103'), null);

    db.checkSession(session_token, function(err, user) {
        return callback(err, user);
    });


    //mongodb.Session.findByIdAndUpdate(session_token, {$set:{updated_at:new Date()}}, {new: true}, function(err, session){
    //    if (err) return callback(err);
    //
    //    if (session) {
    //        mongodb.User.findById(session.user_id, function (err, user) {
    //            if (err) return callback(err);
    //
    //            mongodb.UserConfig.findById(user.config_id, function (err, config) {
    //                if (err) return callback(err);
    //
    //                user.config = config;
    //                mongodb.UserInfo.findById(user.info_id, function (err, info) {
    //                    if (err) return callback(err);
    //
    //                    user.info = info;
    //                    mongodb.UserPermissions.findById(user.permissions_id, function (err, perms) {
    //                        if (err) return callback(err);
    //
    //                        user.permissions = perms;
    //                        return callback(null, user);
    //                    });
    //                });
    //            });
    //        });
    //    } else {
    //        return callback(null, undefined);
    //    }
    //});
};

exports.manageError = function(req, res, context, err) {
    context.title = 'Error'
    context.navHistory.push({
        title: 'Error',
        link: '#'
    });
        res.render('message', {
        context: context,
        title: {
            part1: 'Error ',
            part2: 'Inesperado'
        },
        message: ['¡Ha ocurrido un error inesperado! Estamos trabajando para solucionarlo lo antes posible. Sentimos las molestias.'],
        redirection: {
            link: '/',
            content: 'Volver al inicio'
        }
    });
};

// Token utils
var rand = function() {
    return Math.random().toString(36).substr(2);
};

exports.genToken = function() {
    return rand() + rand();
};

// Crypt utils
var crypto = require('crypto');
exports.hashPass = function(pwd, salt, callback) {
  var hash = crypto.createHash('sha256').update(pwd + salt).digest('base64');
  callback(null, hash);
};

// Html utils
var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

var imgExts = [
    '.jpg',
    '.png',
    '.jpeg',
    '.gif',
    '.bmp',
    '.tiff',
    '.bpg',
    '.webp',
    '.svg',
    '.hdr'
];

exports.escapeHtml = function (string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
};

function replaceURLWithHTMLLinks(text) {
    var exp = /^(?:(?:ht|f)tp(?:s?)\:\/\/|~\/|\/)?(?:\w+:\w+@)?((?:(?:[-\w\d{1-3}]+\.)+(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|edu|co\.uk|ac\.uk|it|fr|tv|museum|asia|local|travel|[a-z]{2}))|((\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)(\.(\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)){3}))(?::[\d]{1,5})?(?:(?:(?:\/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|\/)+|\?|#)?(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?:#(?:[-\w~!$ |\/.,*:;=]|%[a-f\d]{2})*)?$/i;
    return text.replace(exp,"<a href='$1'>$1</a>");
}

exports.parseMessage = function (string) {
    var urlList = linkify.find(string);
    var parsedMsg = exports.escapeHtml(string);
    for (var i = 0; i < urlList.length; i++) {
        var u = urlList[i];
        if (u.type == 'url') {
            var parsedURL = exports.escapeHtml(u.value);
            if (imgExts.indexOf(u.value.substring(u.value.length-4,u.length)) > -1) {
                parsedMsg = parsedMsg.replace(parsedURL, '<img src="' + u.href + '"/>');
            } else {
                parsedMsg = parsedMsg.replace(parsedURL, '<a href="' + u.href + '" target="_blank">' + u.value + '</a>');
            }
        }
    }

    return parsedMsg;
};

exports.formatDate = function(d, skipHours) {
	var dateTime = toTwoDigits(d.getDate()) + '/' + toTwoDigits((d.getMonth()+1)) + '/' + d.getFullYear();
	if (!skipHours) {
		dateTime += ' - ' + toTwoDigits(d.getHours()) + ':' + toTwoDigits(d.getMinutes()) + ':' + toTwoDigits(d.getSeconds());
	}
	return dateTime;
}

function toTwoDigits(num) {
	if (num < 10) {
		return '0' + num;
	}
	return num;
}

exports.parseCookies = function(rc) {
    var list = {};

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}