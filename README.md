# README #

This is the Back-End project of a Spanish speaking community webpage called Animagens.es

### How do I get set up? ###
You will need to install node.js 5.x.x, npm 3.x.x and neo4j in your machine.

Then, you just have to clone the repository, open a console inside the recently downloaded folder and type "npm install" to download all the modules that this project uses.

Also, you will need to create a file in that directory called "config.json" that will include the following structure:

{  
  "test": true,                 // If the run is in "testing" mode

  "server_ip": "127.0.0.1",     // The ip the express server will use to start

  "server_port": "4000",        // The port the express server will use to start

  "cookie_secret": "asdf",      // The secret used by the cookie manager

  "ssl": false,                 // If the connection is going through ssl

  "ips": ["127.0.0.1"],         // IPs allowed

  "mail_secret":"asdf",         // Mail secret for email sending

  "mail_refresh": "asdf"        // Mail refresh token for email sending

}

Then, with a simple "node app.js", the project will start listening for connections.

If you want to test it, it is recommended to download the Front-End project and run them together. You can download it here:
https://bitbucket.org/codeandcoder/ares/overview

### Authors ###
Santi Ruiz starfly1570@gmail.com <codeandcoder.net>