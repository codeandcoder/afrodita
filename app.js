var express = require('express')
    , ipfilter = require('express-ipfilter')
    , bodyParser = require('body-parser')
    , path = require('path')
    , logger = require('./node/logger')
    , middleware = require('./node/middleware')
    , Recaptcha = require('recaptcha').Recaptcha
    , utils = require('./node/utils')
    , db = require('./node/db')
    , config = undefined;

var fs = require('fs');
// Config from params
var lastValue;
process.argv.forEach(function (val, index, array) {
    if (index > 1 && lastValue === '--config-file') {
        config = require('./node/config').vars(val);
    }
    lastValue = val;
});
if (config === undefined) {
    config = require('./node/config').vars('config.json');
}

var ips = config.ips;
var app = express();
var captcha_pubkey = config.captcha_pubkey;
var captcha_prikey = config.captcha_prikey;

// all environments
var cookieParser = require('cookie-parser')();
var session = require('cookie-session')({ secret: config.cookie_secret });
app.set('port', config.server_port);
app.set('ip_addr', config.server_ip);
app.use(require('connect-flash')());
app.use(function (req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
});
app.use(cookieParser);
app.use(session);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
//app.use(ipfilter(ips, {mode: 'allow', log: false}));
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:' + config.server_port);
  res.setHeader('Access-Control-Allow-Methods', 'POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

// Server initialization
var server = undefined;
if (config.ssl) {
    var options = {
        key  : fs.readFileSync('/root/animagens2/ssl/server.key'),
        cert : fs.readFileSync('/root/animagens2/ssl/cagd.crt'),
        ca : fs.readFileSync('/root/animagens2/ssl/bundlegd.crt')
    };
    server = require('https').createServer(options, app);
    // set up plain http server
    var http = express();
    // set up a route to redirect http to https
    http.get('*',function(req,res){
        res.redirect('https://animagens.es'+req.url);
    });
    http.listen(config.server_port);
} else {
    server = require('http').createServer(app);
}

var multer = require('multer');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './public/images/users');
  },
  filename: function (req, file, callback) {
      var session_token = utils.parseCookies(req.headers.cookie).session_token;
      db.checkSession(session_token, function(err, user) {
          if (err || !user) {
              callback({msg: 'No Session!'});
          } else {
              callback(null, user.nickname);
          }
      });
    
  }
});
var upload = multer({storage : storage}).single('file');

app.post('/uploadUserImage', function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.end(err.toString());
    }
    res.end('File is uploaded');
  });
});

app.use(express.static('public'));
app.all('*', (req, res) => {
  res.status(200).sendFile(__dirname + '/public/index.html');
});

var io = require('socket.io')(server);
io.use(function(socket, next) {
    var req = socket.handshake;
    var res = {};
    cookieParser(req, res, function(err) {
        if (err) {
            return next(err);
        }
        session(req, res, next);
    });
});
server.listen(app.get('port'), app.get('ip_addr'), function(){
    logger.info({msg:'Express server listening on port ' + app.get('port')});
});

require('./node/real-time').init(io);
require('./node/neo-checker').init();